from odoo import api, fields, models, _


class ExhangeWizard(models.TransientModel):
    _name = 'exchange'

    line_ids = fields.One2many('exchange.line', 'exchange_id', string='Line')
    load_line = fields.Boolean(string="Select All")

    @api.onchange('load_line')
    def load_all_lines(self):
        if self.load_line:
            sale_rec = self.env['sale.order'].search([('id', '=', self._context.get('active_id', False))])
            self.line_ids = [(0, 0, {'product_ids': item.product_id.id, 'qty': item.product_uom_qty,
                                     'exchange_product_id': item.product_id.id, 'exchange_qty': item.product_uom_qty,
                                     'sale_order_line_id': item.id, 'product_uom': item.product_uom.id}) for item in
                             sale_rec.order_line]
        else:
            self.line_ids = []

    def action_return(self):
        stock_rec = self.env['stock.picking']
        sale_id = self.env['sale.order'].search([('id', '=', self._context.get('active_id', False))])
        sale_rec = self.env['sale.order'].search([('id', '=', self._context.get('active_id', False))]).order_line
        picking_rec = self.env['stock.picking.type'].search([('code', '=', 'incoming')])[0].id
        src_location = self.env['stock.location'].search([('usage', '=', 'customer')], limit=1)
        procurement_id = self.env['procurement.group'].search([('name', '=', sale_id.name)])
        account_rec = self.env['account.invoice']
        vals = []
        out_invoice_vals = []
        refund_invoice_vals = []
        for val in sale_rec:
            for line in self.line_ids:
                if line.sale_order_line_id.id == val.id:
                    line.qty_delivered = val.qty_delivered - line.exchange_qty
                    vals.append((0, 0, {
                        'product_id': line.product_ids.id,
                        'product_uom_qty': line.exchange_qty,
                        'name': line.product_ids.name,
                        'product_uom': line.product_uom.id,
                        'location_id': src_location.id,
                        'group_id': procurement_id.id,
                        'location_dest_id': (
                            self.env['stock.picking.type'].browse(picking_rec).default_location_dest_id).id,
                    }))
                    val.create({
                        'product_id':line.exchange_product_id.id,
                        'product_uom_qty':line.exchange_qty,
                        'price_unit':line.exchange_product_id.list_price,
                        'tax_id':[(6,0,[line.exchange_product_id.taxes_id.id])] if line.exchange_product_id.taxes_id else False ,
                        'order_id':val.order_id.id,
                        'exchange_value':line.product_ids.name +'->'+line.exchange_product_id.name,
                    })
                    # when exchange price greater then actual list price
                    if line.exchange_product_id.list_price > line.product_ids.list_price:
                        out_invoice_vals.append((0,0,{
                            'product_id':line.exchange_product_id.id,
                            'name':line.exchange_product_id.name,
                            'quantity':line.exchange_qty,
                            'price_unit': line.exchange_product_id.list_price - line.product_ids.list_price,
                            'account_id': line.exchange_product_id.property_account_income_id.id or line.exchange_product_id.categ_id.property_account_income_categ_id.id,
                            'invoice_line_tax_ids':[(6,0,[line.exchange_product_id.taxes_id.id])] if line.exchange_product_id.taxes_id else False ,
                            'price_subtotal':line.exchange_product_id.list_price - line.product_ids.list_price
                        }))
                        account_rec.create({
                            'partner_id': sale_id.partner_id.id,
                            'invoice_line_ids': out_invoice_vals,
                            'origin': sale_id.name,
                            'type':'out_invoice',
                        })
                    # when exchange price less then actual list price
                    if line.exchange_product_id.list_price < line.product_ids.list_price:
                        refund_invoice_vals.append((0,0,{
                            'product_id':line.exchange_product_id.id,
                            'name': line.exchange_product_id.name,
                            'quantity':line.exchange_qty,
                            'account_id': line.exchange_product_id.property_account_income_id.id or line.exchange_product_id.categ_id.property_account_income_categ_id.id,
                            'price_unit': line.product_ids.list_price - line.exchange_product_id.list_price,
                            'invoice_line_tax_ids':[(6,0,[line.exchange_product_id.taxes_id.id])] if line.exchange_product_id.taxes_id else False ,
                            'price_subtotal':line.product_ids.list_price - line.exchange_product_id.list_price
                        }))
                        account_rec.create({
                            'partner_id': sale_id.partner_id.id,
                            'invoice_line_ids': refund_invoice_vals,
                            'origin': sale_id.name,
                            'type': 'out_refund',
                        })
                    # when exchange price equal to the  actual list price
                    # if line.exchange_product_id.list_price == line.product_ids.list_price:
                    #     account_inv = self.env[]
        stock_id = stock_rec.create({
            'name':self.env['stock.picking.type'].browse(picking_rec).sequence_id.next_by_id(),
            'partner_id': sale_id.partner_id.id,
            'picking_type_id': picking_rec,
            'sale_id': self._context.get('active_id', False),
            'location_id': 12,  # (self.env['stock.picking.type'].browse(picking_rec).default_location_src_id).id,
            'move_type': 'direct',
            'location_dest_id': (self.env['stock.picking.type'].browse(picking_rec).default_location_dest_id).id,
            'move_ids_without_package': vals,
            'origin': sale_id.name,
            
        })
        print("fasasdsdfsdfhsdfdsd", stock_id, procurement_id.name)

        # for line in sale_rec:
        #     for val in self.line_ids:
        #         if val.sale_order_line_id == line.id:
        #             line.qty_delivered = line.qty_delivered - val.exchange_qty
        #             stock=stock_rec.create({
        #                 #'name':self.env['stock.picking.type'].browse(picking_rec).sequence_id.next_by_id(),
        #                 'partner_id':line.order_id.partner_id.id,
        #                 'picking_type_id':3,
        #                 'sale_id': self._context.get('active_id', False),
        #                 'location_id':12, #(self.env['stock.picking.type'].browse(picking_rec).default_location_src_id).id,
        #                 'move_type': 'direct',
        #                 'location_dest_id': (self.env['stock.picking.type'].browse(picking_rec).default_location_dest_id).id,
        #                 'company_id': self.env.user.company_id.id,
        #                 'move_ids_without_package':[(0, 0,{
        #                     'product_id': val.product_ids.id,
        #                     'product_uom_qty':line.qty_delivered,
        #                     'name':val.product_ids.name,
        #                     'product_uom':line.product_uom.id,
        #                     'location_id':src_location.id,
        #                     'location_dest_id':(self.env['stock.picking.type'].browse(picking_rec).default_location_dest_id).id,
        #                     })]

        #             })
        #             print("stockstockstockstock",stock)


class ExchangeLine(models.TransientModel):
    _name = 'exchange.line'

    product_ids = fields.Many2one('product.product', string='Product Name')
    qty = fields.Integer('Quantity')
    exchange_id = fields.Many2one('exchange')
    exchange_product_id = fields.Many2one('product.product', string='Exchange Product Name')
    exchange_qty = fields.Integer('Exchange Quantity')
    sale_order_line_id = fields.Many2one('sale.order.line')
    product_uom = fields.Many2one('uom.uom')