# -*- coding: utf-8 -*-

from odoo import models, fields, api

class SaleOrderInherit(models.Model):
    _inherit = 'sale.order'

    def action_product_exchange(self):
        [action] = self.env.ref('delivery_exchange.action_wizard_delivery_exchange').read()
        action['view_mode'] = 'form'
        action['view_mode'] = 'form'
        action['target'] = 'new'
        action['context'] = {'default_load_line': True}
        return action



class SaleOrderLineInherit(models.Model):
    _inherit = 'sale.order.line'

    exchange_value = fields.Char('Exchange Value')