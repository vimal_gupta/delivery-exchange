# -*- coding: utf-8 -*-
{
    'name': "delivery_exchange",


    'description': """
        This module track the delivery exchange of the product
    """,

    'author': "IqMinds Technology",
    'website': "http://www.iqminds.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Warehouse',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
	    'wizard/exchange_wizard.xml',
    ],
    # only loaded in demonstration mode
}
